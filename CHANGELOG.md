## 0.3.4 - 2024-05-21
* Mark project as abandoned

## 0.3.3 - 2022-04-18
* Bump dependencies, especially regex for RUSTSEC-2022-0013.

## 0.3.2 - 2022-02-01
* Bump dependencies to fix RUSTSEC-2020-0159 and RUSTSEC-2022-0006.

## 0.3.1 - 2021-10-28
* Bump dependencies, especially tracing-subscriber to reduce dependence
  on chrono, which is not thread-safe.

## 0.3.0 - 2021-10-06
* [BREAKING] Implement remote health checks. curl must be installed on the
  remote host now.
* Bump dependencies

## 0.2.1 - 2021-09-27
* Publish amd64 and armv7 binaries via GitLab releases
* Bump dependencies

## 0.2.0 - 2021-08-22
* Switch to `tracing` crate for logging
* Expose prometheus metrics via HTTP server on port 46581

## 0.1.0 - 2021-08-07
* Initial release
