multi-tunnel
============

> This project is abandoned; use something like tailscale instead.

multi-tunnel allows starting and managing SSH tunnels in a
straightforward manner. Create a TOML configuration file,
start the service and you're all set. If you change the
configuration, send a SIGHUP (aka `systemctl reload`)
and it'll start/stop tunnels as necessary.

Tunnels that get stopped or killed for any reason will be
restarted after a 5-second delay. Every 60s it will SSH into
the host to run health checks to make sure the ports are accessible
over HTTP. Note that curl must be installed on the remote host.

## Configuration

For example, to tunnel httpd running on port 8080 to
a remote server on 8000 and nginx running on port 8081
to 8001 on the remote server:

```toml
connection = "remote.host"

[httpd]
local = 8080
remote = 8000

[nginx]
local = 8081
remote = 8001
```

The name is arbitrary and is used only for log entries.

## Monitoring
A Prometheus endpoint to scrape is available at
`http://0.0.0.0:46581/metrics`.

## License
multi-tunnel is dual licensed under the GPL v3 or later OR
copyleft-next 0.3.1, see LICENSES/.
